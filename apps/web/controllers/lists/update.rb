module Web::Controllers::Lists
  class Update
    include Web::Action
    accept :json
    expose :lists

    params do
      required(:name).filled
    end

    def call(params)
      @list_data = list_repository.find(params[:id])

      if @list_data.nil?
        self.status = 422
        self.body = "Something went wrong".to_json
      else
        @list = list_repository.update(@list_data.id, name: params[:name])
        self.status = 200
        self.body = @list.to_h.to_json
      end
    end

    def list_repository
      @list_repository ||= ListRepository.new
    end
  end
end
