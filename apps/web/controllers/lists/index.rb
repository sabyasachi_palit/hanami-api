module Web::Controllers::Lists
  class Index
    include Web::Action
    accept :json
    expose :lists

    def call(params)
      self.status = 200
      self.body = ListRepository.new.all.map{|list| list.to_h}.to_json
    end

    def list_repository
      @list_repository ||= ListRepository.new
    end
  end
end
