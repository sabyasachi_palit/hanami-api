module Web::Controllers::Lists
  class Create
    include Web::Action
    accept :json
    expose :lists

    params do
      required(:name).filled
    end

    def call(params)
      if params.valid?
        self.status = 200
        @list = list_repository.create(params)
        self.body = @list.to_h.to_json
      else
        self.status = 422
        self.body = "Something went wrong".to_json
      end
    end

    def list_repository
      @list_repository ||= ListRepository.new
    end
  end
end
