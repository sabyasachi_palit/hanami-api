module Web::Controllers::Lists
  class Destroy
    include Web::Action
    accept :json
    expose :lists

    def call(params)
      @list_data = list_repository.find(params[:id])

      if @list_data.nil?
        self.status = 201
        self.body = "No data found".to_json
      else
        @list = list_repository.delete(params[:id])

        if @list.nil?
          self.status = 201
          self.body = "Something went wrong".to_json
        else
          self.status = 200
          self.body = "Data Deleted".to_json
        end
      end
    end

    def list_repository
      @list_repository ||= ListRepository.new
    end
  end
end
