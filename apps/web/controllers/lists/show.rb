module Web::Controllers::Lists
  class Show
    include Web::Action
    accept :json
    expose :lists

    def call(params)
      @list = list_repository.find(params[:id])

      if @list.nil?
        self.status = 201
        self.body = "No data found".to_json
      else
        self.status = 200
        self.body = @list.to_h.to_json
      end
    end

    def list_repository
      @list_repository ||= ListRepository.new
    end
  end
end
